package timetracker;

//TODO Alles wat met dit geregistreerd moet globaal gemaakt worden door middel van een observer
import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TimeAddDialog extends JDialog implements ActionListener {
	private static final long serialVersionUID = 5293646464458363462L;
	
	private final JPanel contentPanel = new JPanel();
	
	/* De widgets */
	private JTextField textField;
	private JButton cancelButton;
	private JButton okButton;

	/**
	 * Create the dialog.
	 */
	public TimeAddDialog() {
		
		setAlwaysOnTop(true);
		setModal(true);
		setResizable(false);
		setModalityType(ModalityType.APPLICATION_MODAL);
		setBounds(100, 100, 308, 91);
		setTitle("Entry toevoegen");
		
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(10, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		
		
		GridBagLayout gbl_contentPanel = new GridBagLayout();
		gbl_contentPanel.columnWidths = new int[]{123, 96, 0};
		gbl_contentPanel.rowHeights = new int[]{20, 0};
		gbl_contentPanel.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gbl_contentPanel.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		contentPanel.setLayout(gbl_contentPanel);
		
		JLabel lblNaamVanDe = new JLabel("Naam van de entry:");
		GridBagConstraints gbc_lblNaamVanDe = new GridBagConstraints();
		gbc_lblNaamVanDe.anchor = GridBagConstraints.WEST;
		gbc_lblNaamVanDe.insets = new Insets(0, 0, 0, 5);
		gbc_lblNaamVanDe.gridx = 0;
		gbc_lblNaamVanDe.gridy = 0;
		contentPanel.add(lblNaamVanDe, gbc_lblNaamVanDe);

		textField = new JTextField();
		textField.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.anchor = GridBagConstraints.NORTH;
		gbc_textField.gridx = 1;
		gbc_textField.gridy = 0;
		contentPanel.add(textField, gbc_textField);
		textField.setColumns(10);

		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
	
		okButton = new JButton("OK");
		okButton.setActionCommand("OK");
		okButton.addActionListener(this);
		buttonPane.add(okButton);
		getRootPane().setDefaultButton(okButton);
		
		cancelButton = new JButton("Cancel");
		cancelButton.setActionCommand("Cancel");
		cancelButton.addActionListener(this);
		buttonPane.add(cancelButton);
	}

	@Override
	public void actionPerformed(ActionEvent e) {		
		Object ob = e.getSource();
		if(ob == cancelButton) {
			this.setVisible(false);
		} else if(ob == okButton) {
			if(this.textField.getText().equals("")) {
				JOptionPane.showMessageDialog(this, "je moet een naam opgeven", "Error", JOptionPane.ERROR_MESSAGE);
			} else {
				TimeEntry tmpentry = new TimeEntry(this.textField.getText(), System.currentTimeMillis());
				TimeBook.getInstance().addEntry(tmpentry);
				this.setVisible(false);
			}
		}
	}
}
