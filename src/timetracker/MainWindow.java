package timetracker;

import java.awt.BorderLayout;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JTable;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.KeyStroke;

import java.awt.event.KeyEvent;
import java.awt.event.InputEvent;
import java.awt.print.PrinterException;
import java.util.ArrayList;

import javax.swing.JScrollPane;
import javax.swing.JOptionPane;

public class MainWindow extends JFrame implements Observer {
	private static final long serialVersionUID = -1972403510895953734L;
	
	private JPanel contentPane;
	private MainTableModel tablemodel;
	private EntryBookWindow bookwindow = new EntryBookWindow();
	private JTable timetable;
	
	public static class MainTableModel implements TableModel {
		ArrayList<TimeEntry> values = new ArrayList<TimeEntry>();
		
		@Override
		public void addTableModelListener(TableModelListener l) {
			// TODO Wat betekent dit?
			
		}

		@Override
		public Class<?> getColumnClass(int columnIndex) {
			return Object.class;
		}

		@Override
		public int getColumnCount() {
			return 4;
		}

		@Override
		public String getColumnName(int columnIndex) {
			String retval = null;
			switch(columnIndex) {
			case 0:
				retval = "ID";
				break;
			case 1:
				retval = "Naam";
				break;
			case 2:
				retval = "Begin";
				break;
			case 3:
				retval = "Einde";
				break;
				
			}
			
			return retval;
		}

		@Override
		public int getRowCount() {
			return values.size();
		}

		@Override
		public Object getValueAt(int rowIndex, int columnIndex) {
			Object retval = null;
			
			TimeEntry tmprow = values.get(rowIndex);
			
			switch(columnIndex) {
			case 0:
				retval = tmprow.getId();
				break;
			case 1:
				retval = tmprow.getNaam();
				break;
			case 2:
				retval = tmprow.getBeginAsDateString();
				break;
			case 3:
				retval = tmprow.getEndAsDateString();
				break;
			}
			
			
			return retval;
		}

		@Override
		public boolean isCellEditable(int rowIndex, int columnIndex) {
			//Geen enkele cel kan veranderd worden
			return false;
		}

		@Override
		public void removeTableModelListener(TableModelListener l) {
			// TODO Wat betekent dit?
			
		}

		@Override
		public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
			//niets aangezien er toch geen enkele cel veranderd kan worden
		}
		
		public void addEntry(TimeEntry entry) {
			values.add(entry);
		}
		
		public void clearEntries() {
			values.clear();
		}
		
	}

	/**
	 * Create the frame.
	 */
	public MainWindow() {
		setTitle("Pieters timetracker");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		
		//bookwindow inschrijven op timebook
		TimeBook.getInstance().registerObserver(bookwindow);
		
		JMenuBar mainmenu = new JMenuBar();
		setJMenuBar(mainmenu);
		
		JMenu mnFile = new JMenu("File");
		mainmenu.add(mnFile);
		
		JMenuItem afsluititem = new JMenuItem("Afsluiten");
		afsluititem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, InputEvent.CTRL_MASK));
		afsluititem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		
		JMenuItem mntmOplsaan = new JMenuItem("Oplsaan");
		mntmOplsaan.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));
		mntmOplsaan.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				//JOptionPane.showMessageDialog(MainWindow.this, "nog niet geimplementeerd", "info", JOptionPane.WARNING_MESSAGE);
				JFileChooser fc = new JFileChooser();
				fc.showSaveDialog(MainWindow.this);
				
				String selected = fc.getSelectedFile().getAbsolutePath();
				boolean saved = TimeBook.getInstance().saveToXML(selected);
				
				if(!saved) {
					JOptionPane.showMessageDialog(MainWindow.this, "niet bewaard", "info", JOptionPane.WARNING_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(MainWindow.this, "succesvol bewaard", "info", JOptionPane.INFORMATION_MESSAGE);
				}
				
			}
		});
		
		JMenuItem mntmOpenen = new JMenuItem("Openen");
		mntmOpenen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser fc = new JFileChooser();
				int result = fc.showOpenDialog(MainWindow.this);
				if (result == JFileChooser.APPROVE_OPTION) {
					String filetopen = fc.getSelectedFile().getAbsolutePath();
					boolean loaded = TimeBook.getInstance().loadFromXML(
							filetopen);
					if (!loaded) {
						JOptionPane.showMessageDialog(MainWindow.this,
								"Bestand niet geladen", "Error",
								JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});
		mntmOpenen.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_MASK));
		mnFile.add(mntmOpenen);
		mnFile.add(mntmOplsaan);
		mnFile.add(afsluititem);
		
		JMenu mnRecords = new JMenu("Records");
		mainmenu.add(mnRecords);
		
		JMenuItem toevoegitem = new JMenuItem("Toevoegen");
		mnRecords.add(toevoegitem);
		toevoegitem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_T, InputEvent.CTRL_MASK));
		
		JMenuItem recorditem = new JMenuItem("Records tonen");
		mnRecords.add(recorditem);
		recorditem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, InputEvent.CTRL_MASK));
		
		JMenuItem mntmPrinten = new JMenuItem("Printen");
		mnRecords.add(mntmPrinten);
		mntmPrinten.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					timetable.print();
				} catch (PrinterException e) {
					JOptionPane.showMessageDialog(MainWindow.this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					e.printStackTrace();
				}
			}
		});
		mntmPrinten.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, InputEvent.CTRL_MASK));
		recorditem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				bookwindow.setVisible(true);
			}
		});
		toevoegitem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new TimeAddDialog().setVisible(true);
			}
		});
		contentPane = new JPanel();
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		tablemodel = new MainWindow.MainTableModel();
		
		JScrollPane mainscroller = new JScrollPane();
		contentPane.add(mainscroller, BorderLayout.CENTER);
		
		timetable = new JTable();
		timetable.setModel(tablemodel);
		mainscroller.setViewportView(timetable);
		mainscroller.setColumnHeaderView(timetable.getTableHeader());
		
		//inschrijven in timebook
		TimeBook.getInstance().registerObserver(this);
	}

	@Override
	public void update() {
		TimeBook tmpbook = TimeBook.getInstance();
		ArrayList<TimeEntry>tmplist = tmpbook.getAllEntries();
		
		tablemodel.clearEntries();
		
		for(TimeEntry e: tmplist) {
			tablemodel.addEntry(e);
		}
		timetable.updateUI();
	}

}
