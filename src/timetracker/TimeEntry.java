package timetracker;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Een klasse voor 1 lijn in een tijdboek
 * @author Pieter Van Keymeulen
 *
 */

public class TimeEntry {
	private String naam;
	private Date begin;
	private Date einde;
	
	private int id;
	private boolean stopped;
	
	public TimeEntry(int id, String naam, long begin, long einde, boolean stopped) {
		this.naam = naam;
		this.id = id;
		this.stopped = stopped;
		this.einde = new Date(einde);
		this.begin = new Date(begin);
	}
	
	public TimeEntry(String beschrijving, long begin) {
		
		this.naam = beschrijving;
		this.begin = new Date(begin);
		
		if(TimeBook.getInstance().getAllEntries().isEmpty()) {
			this.id = 1;
		} else {
			this.id = TimeBook.getInstance().getHighestId() + 1;
		}
		
		
		this.stopped = false;
		
	}

	public String getNaam() {
		return naam;
	}

	public Date getBegin() {
		return begin;
	}
	
	public Date getEind() {
		return einde;
	}

	public int getId() {
		return id;
	}
	
	public void stopEntry() {
		this.stopped = true;
		//TODO die 3600000 moet weg.
		this.einde = new Date((System.currentTimeMillis() - begin.getTime()) - 3600000);
	}
	
	public boolean isStopped() {
		return stopped;
	}
	
	/**
	 * Geeft het de begintijd van deze entry als een datestring terug
	 * @return een datestring
	 */
	public String getBeginAsDateString() {
		DateFormat df = new SimpleDateFormat("HH:mm:ss");
		return df.format(begin);
	}
	
	/**
	 * Geeft het verschil van de begintijd van deze entry met de tegenwoordige tijd
	 * als een datestring
	 * Dit werkt niet zo goed: mogelijke oplossing multitrheading of tasks
	 * @return een datestring
	 */
	public String getEndAsDateString() {
		String retval;
		
		if(einde == null) {
			retval = "niet gestopt";
		} else {
			DateFormat df = new SimpleDateFormat("HH:mm:ss");
			retval = df.format(einde);
		}
		
		return retval;
	}

	@Override
	public String toString() {
		return "TimeEntry [naam=" + naam + ", begin=" + begin + ", id=" + id
				+ ", stopped=" + stopped + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((naam == null) ? 0 : naam.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TimeEntry other = (TimeEntry) obj;
		if (id != other.id)
			return false;
		if (naam == null) {
			if (other.naam != null)
				return false;
		} else if (!naam.equals(other.naam))
			return false;
		return true;
	}
	
	
}
