package timetracker;

import java.util.ArrayList;

public class TimeBook implements Subject {
	ArrayList<Observer> observers;
	ArrayList<TimeEntry> entries;
	
	private static TimeBook instance = new TimeBook();
	
	//er een singleton van maken
	private TimeBook() {
		observers = new ArrayList<Observer>();
		entries = new ArrayList<TimeEntry>();
	}
	
	public static TimeBook getInstance() {
		return instance;
	}
	
	
	public boolean addEntry(TimeEntry entry) {
		boolean retval = false;
		
		if(!entries.contains(entry)) {
			this.entries.add(entry);
			this.notifyObservers();
			retval = true;
		}
		
		return retval;
	}
	
	public boolean stopEntryByName(String name) {
		boolean retval = false;
		
		for(TimeEntry e: entries) {
			if(e.getNaam() == name) {
				e.stopEntry();
				retval = true;
				notifyObservers();
			}
		}
		
		return retval;
	}
	
	public int getNumberOfEntries() {
		return entries.size();
	}
	
	public ArrayList<TimeEntry> getAllEntries() {
		
		// geen idee of hier deze kloon wel nodig is
		ArrayList<TimeEntry> retval = new ArrayList<TimeEntry>();
		for(TimeEntry e: entries) {
			retval.add(e);
		}
		
		return retval;
	}
	
	public TimeEntry getEntryById(int id) {
		for(TimeEntry e: entries) {
			if(e.getId() == id) {
				return e;
			}
		}
		
		return null;
	}
	
	//TODO ik weet niet of dit werkt
	public boolean removeEntryById(int id) {
		for(TimeEntry t: entries) {
			if(t.getId() == id) {
				entries.remove(t);
				this.notifyObservers();
				return true;
			}
		}
		return false;
	}
	
	//TODO ervoor zorgen dat een beschrijving uniek is
	public TimeEntry getEntryByNaam(String naam) {
		for(TimeEntry t: entries) {
			if(t.getNaam() == naam) {
				return t;
			}
		}
		
		return null;
	}
	
	public boolean removeEntryByNaam(String naam) {
		for(TimeEntry e: entries) {
			if(e.getNaam() == naam) {
				entries.remove(e);
				this.notifyObservers();
				return true;
			}
		}
		return false;
	}
	
	public int getHighestId() {
		int retval = 0;
		
		for(TimeEntry e: entries) {
			if(e.getId() > retval)
				retval = e.getId();
			
		}
		
		return retval;
	}
	
	public boolean saveToXML(String filename) {
		//TODO: HIER EEN GOEDE IMPLEMENTATIE AAN GEVEN
		return false;
	}
	
	public boolean loadFromXML(String filename) {
		//TODO: HIER EEN GOEDE IMPLEMENTATIE AAN GEVEN
		return false;
	}

	@Override
	public void registerObserver(Observer ob) {
		observers.add(ob);
		//ik zou dit hier weg willen
		this.notifyObservers();
	}

	@Override
	public void unregisterObserver(Observer ob) {
		observers.remove(ob);
	}

	@Override
	public void notifyObservers() {
		for(Observer ob: observers)	{
			ob.update();
		}
	}

}
