package timetracker.database;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import timetracker.TimeEntry;

public class TimeEntryDAO {
	private static Connection conn = null;
	
	static {
		try {
			if(conn == null || conn.isClosed()) {
				conn = SQLSingleton.getInstance().getConnection();
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public static ArrayList<TimeEntry> getAll() {
		ArrayList<TimeEntry> retval = new ArrayList<TimeEntry>();
		Statement stmt;
		try {
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("select * from TimeTable");
			
			while(rs.next()) {
				TimeEntry tmp = new TimeEntry(rs.getInt(1), rs.getString(2), rs.getDate(3).getTime(), rs.getDate(4).getTime(), rs.getBoolean(5));
				
				retval.add(tmp);
			}
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}

		return retval;
	}
	
	public static ArrayList<TimeEntry> getEntriesByNaam(String naam) {
		ArrayList<TimeEntry> retval = new ArrayList<TimeEntry>();
		PreparedStatement ps;
		ResultSet rs;
		
		try {
			ps = conn.prepareStatement("select * from TimeTable where naam = ?");
			ps.setString(1, naam);
			rs = ps.executeQuery();
			
			while(rs.next()) {
				retval.add(new TimeEntry(rs.getInt(1), rs.getString(2), rs.getDate(3).getTime(), rs.getDate(4).getTime(), rs.getBoolean(5)));
			}
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			ex.printStackTrace();
		}
		
		
		return retval;
	}
	
	public static TimeEntry getEntryById(int id) {
		PreparedStatement ps;
		ResultSet rs;
		TimeEntry retval = null;
		
		try {
			ps = conn.prepareStatement("select * from TimeTable where id = ?");
			ps.setInt(1, id);
			rs = ps.executeQuery();
			
			while(rs.next()) {
				retval = new TimeEntry(rs.getInt(1), rs.getString(2), rs.getDate(3).getTime(), rs.getDate(4).getTime(), rs.getBoolean(5));
			}
		} catch (SQLException ex) {
				ex.printStackTrace();
		}
		return retval;
	}
	
	public static boolean saveEntry(TimeEntry entry) {
		boolean retval = true;
		
		try {
			PreparedStatement ps = conn.prepareStatement("insert into TimeTable values(?, ?, ?, ?, ?)");
			ps.setInt(1, entry.getId());
			ps.setString(2, entry.getNaam());
			ps.setDate(3, new Date(entry.getBegin().getTime()));
			ps.setDate(4, new Date(entry.getEind().getTime()));
			ps.setBoolean(5, entry.isStopped());
		} catch (SQLException e) {
			retval = false;
			e.printStackTrace();
		}
		
		
		return retval;
	}
}
