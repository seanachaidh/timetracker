package timetracker.database;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.TreeMap;

public class SQLSingleton {
	private Connection conn;
	private TreeMap<String, String> config = new TreeMap<String, String>();
	
	private static SQLSingleton instance = new SQLSingleton();
	
	private SQLSingleton() {
		BufferedReader tmpfile = new BufferedReader(new InputStreamReader(this.getClass().getResourceAsStream("/timetracker/config/dbconfig.conf")));
		String tmpconnstring = "jdbc:";
		
		String line;
		try {
			while((line = tmpfile.readLine()) != null) {
				String[] tmpconfline = line.split(":");
				config.put(tmpconfline[0], tmpconfline[1]);
			}
		} catch (IOException ex) {
			System.out.println(ex.getMessage());
		}
		
		if(config.get("databasetype").equals("sqlite")) {
			tmpconnstring += "sqlite:" + config.get("databasehost");
		} else if (config.get("databasetype").equals("mysql")) {
			tmpconnstring += "mysql://" + config.get("databasehost");
		}
		
		try {
			conn = DriverManager.getConnection(tmpconnstring, config.get("databaseuser"), config.get("databasepass"));
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
		//initiele configuratie
		Statement stmt;
		try {
			stmt = conn.createStatement();
			stmt.execute("create table if not exists TimeTable(id integer primary key, naam varchar(30), entry_begin date, entry_einde date)");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
	}
	
	public Connection getConnection() {
		return conn;
	}
	
	public static SQLSingleton getInstance() {
		return instance;
	}
	
}
