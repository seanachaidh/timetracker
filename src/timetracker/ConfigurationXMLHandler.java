package timetracker;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class ConfigurationXMLHandler extends DefaultHandler {
	
	private String currentElement;
	private String gebruikersnaam;
	private String database;
	private String prijsperuur;
	
	@Override
	public void startElement(String uri, String localname, String qname,
			Attributes atts) throws SAXException {
		currentElement = qname;
		
	}

	@Override
	public void characters(char[] arg0, int start, int length) throws SAXException {
		if(this.currentElement.equals("gebruikersnaam")) {
			this.gebruikersnaam = arg0.toString();
		} else if (this.currentElement.equals("database")) {
			this.database = arg0.toString();
		} else if (this.currentElement.equals("prijsperuur")) {
			this.prijsperuur = arg0.toString();
		}
	}

}
