create table if not exists TimeTable(
	id integer primary key,
	naam varchar(30),
	entry_begin date,
	entry_einde date
);
