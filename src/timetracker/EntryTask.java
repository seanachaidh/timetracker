package timetracker;

import java.util.Date;
import java.util.TimerTask;

public class EntryTask extends TimerTask {
	
	private int secs;
	
	@Override
	public void run() {
		//hier zit voorlopig nog niets in
		while(true) {
			secs++;
		}
	}
	
	public Date toDate() {
		return new Date(secs * 1000);
	}
	
	@Override
	public String toString() {
		String retval = "";
		int uren = 0, minuten = 0, seconden = 0;
		
		uren = secs / 3600;
		secs -= uren * 3600;
		minuten = secs / 60;
		secs -= minuten * 60;
		
		seconden = secs;
		
		
		retval += uren + ":" + minuten + ":" + seconden;
		
		return retval;
	}
	
}
