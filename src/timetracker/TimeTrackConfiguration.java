package timetracker;

public class TimeTrackConfiguration {
	private String gebruikersnaam;
	private double prijsperuur;
	private String filename;
	
	private TimeTrackConfiguration(String filename) {
		
	}
	
	public static TimeTrackConfiguration FromFile(String filename) {
		return new TimeTrackConfiguration(filename);
	}
	
	public String getGebruikersnaam() {
		return gebruikersnaam;
	}
	public void setGebruikersnaam(String gebruikersnaam) {
		this.gebruikersnaam = gebruikersnaam;
	}
	public double getPrijsperuur() {
		return prijsperuur;
	}
	public void setPrijsperuur(double prijsperuur) {
		this.prijsperuur = prijsperuur;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TimeTrackConfiguration [gebruikersnaam=")
				.append(gebruikersnaam).append(", prijsperuur=")
				.append(prijsperuur).append(", filename=").append(filename)
				.append("]");
		return builder.toString();
	}
	
}
