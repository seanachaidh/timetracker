package timetracker;

import java.awt.BorderLayout;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.ListModel;
import javax.swing.event.ListDataListener;
import javax.swing.JList;
import javax.swing.JButton;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class EntryBookWindow extends JFrame implements Observer, ActionListener {
	private static final long serialVersionUID = 8493765397513803799L;
	private EntryBookWindow.FrameListModel model = new FrameListModel();
	
	public static class FrameListModel implements ListModel{
		private ArrayList<String> values;
		
		public FrameListModel() {
			values = new ArrayList<String>();
		}
		
		@Override
		public void addListDataListener(ListDataListener arg0) {
			//TODO WAT BETEKENT DIT?
		}

		@Override
		public String getElementAt(int arg0) {
			return values.get(arg0);
		}

		@Override
		public int getSize() {
			return values.size();
		}

		@Override
		public void removeListDataListener(ListDataListener arg0) {
			//TODO WAT BETEKENT DIT?
		}
		
		public void addElement(String elem) {
			values.add(elem);
		}
		
		public void setNewList(ArrayList<String> list) {
			this.values = list;
		}
		
		public void clearList() {
			values.clear();
		}
		
	}
	
	private JPanel contentPane;
	private JList mainlist;
	private JButton closebutton;
	private JButton removebutton;
	private JButton stopbutton;

	/**
	 * Create the frame.
	 */
	public EntryBookWindow() {
		setTitle("Entry list");
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		mainlist = new JList();
		mainlist.setModel(model);
		
		contentPane.add(mainlist, BorderLayout.CENTER);
		
		JPanel menupanel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) menupanel.getLayout();
		flowLayout.setAlignment(FlowLayout.RIGHT);
		contentPane.add(menupanel, BorderLayout.SOUTH);
		
		removebutton = new JButton("Verwijderen");
		removebutton.addActionListener(this);
		menupanel.add(removebutton);
		
		stopbutton = new JButton("Stoppen");
		stopbutton.addActionListener(this);
		menupanel.add(stopbutton);
		
		
		closebutton = new JButton("Sluiten");
		closebutton.addActionListener(this);
		menupanel.add(closebutton);
	}

	@Override
	public void update() {
		TimeBook tmpbook = TimeBook.getInstance();
		ArrayList<TimeEntry> tmplist = tmpbook.getAllEntries();
		model.clearList();
		
		for(TimeEntry e: tmplist) {
			model.addElement(e.getNaam());
		}
		
		mainlist.updateUI();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object ob = e.getSource();
		
		if(ob == this.closebutton) {
			this.setVisible(false);
		} else if(ob == this.removebutton) {
			if(mainlist.getSelectedIndex() < 0) {
				JOptionPane.showMessageDialog(this, "Geen entry geselecteerd", "Error", JOptionPane.ERROR_MESSAGE);
			} else {
				String selected = model.getElementAt(mainlist.getSelectedIndex());
				System.out.println("verwijderen: " + selected);
				TimeBook.getInstance().removeEntryByNaam(selected);				
			}

		} else if(ob == stopbutton) {
			if(mainlist.getSelectedIndex() < 0){
				JOptionPane.showMessageDialog(this, "Geen entry geselecteerd", "Error", JOptionPane.ERROR_MESSAGE);
			} else {
				String selected = model.getElementAt(mainlist.getSelectedIndex());
				TimeBook.getInstance().stopEntryByName(selected);				
			}

		}
		
	}

}
