package timetracker;

public interface Observer {
	void update();
}
