package timetracker;

import static org.junit.Assert.*;

import java.lang.reflect.Field;

import org.junit.Before;
import org.junit.Test;

public class EntryTaskTest {
	private EntryTask testobj;
	private Field secsfield;
	
	@Before
	public void setUp() throws Exception {
		testobj = new EntryTask();
		
		secsfield = EntryTask.class.getDeclaredField("secs");
		secsfield.setAccessible(true);
	}
	
	

	@Test
	public void testToString() {
		try {
			secsfield.set(testobj, (Integer) 1200);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		assertEquals("0:20:0", testobj.toString());
	}

}
